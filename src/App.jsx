import * as React from 'react';

import OlMap from 'ol/Map';
import OlView from 'ol/View';
import OlLayerTile from 'ol/layer/Tile';
import OlSourceOsm from 'ol/source/OSM';
import TileWMS from 'ol/source/TileWMS';

import {
  CircleMenu,
  SimpleButton,
  MapComponent,
  MapProvider,
  NominatimSearch,
  mappify
} from '@terrestris/react-geo';

const Map = mappify(MapComponent);
const Search = mappify(NominatimSearch);

class App extends React.Component {

  constructor(props) {

    super(props);

    this.mapDivId = `map-${Math.random()}`;

    this.mapPromise = new Promise(resolve => {
      // const layer = new OlLayerTile({
      //   source: new OlSourceOsm()
      // });

      // const map = new OlMap({
      //   target: null,
      //   view: new OlView({
      //     center: [
      //       135.1691495,
      //       34.6565482
      //     ],
      //     projection: 'EPSG:4326',
      //     zoom: 16,
      //   }),
      //   layers: [layer]
      // });
      const map = new OlMap({
        layers: [new OlLayerTile({
          // extent: [-0.0035, -0.0013, 0.1996, 0.1012],
          // extent: [-7.883615017655093, -3.5603439725046457, -7.8836136434676565, -3.5603431412661326],
          extent: [-0.40111721647085197, -0.06188326091302604, 0.7079921401457115, 0.1711229064433949],
          visible: true,
          source: new TileWMS({
            url: 'http://teardown.home:8080/geoserver/wms',
            extent: [-0.40111721647085197, -0.06188326091302604, 0.7079921401457115, 0.1711229064433949],
            // extent: [-0.0035, -0.0013, 0.1996, 0.1012],
            // extent: [-7.883615017655093, -3.5603439725046457, -7.8836136434676565, -3.5603431412661326],
            params: {
              'FORMAT': 'image/png',
              'VERSION': '1.1.1',
              tiled: true,
              "STYLES": '',
              "LAYERS": 'reverse-costing:teardowns_devicecomponent_georeference',
              "exceptions": 'application/vnd.ogc.se_inimage',
              tilesOrigin: -0.0035 + "," + -0.0013
            }
          })
        })],
        target: null,
        view: new OlView({
          projection: 'EPSG:3947',
          zoom: 27,
          maxZoom: 33,
          center: [0.09805, 0.04995],
        }),
      });

      this.map = map;

      resolve(map);
    });
  }


  componentDidMount() {
    console.log('map id ', this.mapDivId);
    window.setTimeout(() => {
      this.map.setTarget(this.mapDivId);
    }, 100);
  }

  render() {
    return (
      <MapProvider map={this.mapPromise}>
        {/* NominatimSearch:
      <Search /> */}
        <Map
          id={this.mapDivId}
          style={{
            height: '400px'
          }}
        />
      </MapProvider>
    );
  }
}
export default App;