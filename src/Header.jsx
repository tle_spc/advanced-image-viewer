import React, { Component } from 'react'
import logo from './images/systemplusconsulting_logo.png'

class Header extends Component {
  render() {
    return (
      <React.Fragment>
        <div id="header">
          <a id="brand" href="/">
            <img style={{ height: "39px" }} id="logo" src={logo} alt="Teardowns by System Plus Consulting" />
          </a>
          <div id="menu-teardowns">
            <div class="title"><a href="/teardowns/?q=&category=Phones&category=Smart+Home&category=Wearables&category=Tablet,+Computing+%26+Gaming&category=Accessory&show_only_full_teardowns=on/">Consumer</a></div>
            <ul>
              <li><a href="/teardowns/?q=&category=Phones&category=Accessory&show_only_full_teardowns=on">Phone</a></li>
              <li><a href="/teardowns/?q=&category=Smart+Home&show_only_full_teardowns=on">Smart Home</a></li>
              <li><a href="/teardowns/?q=&category=Wearables&show_only_full_teardowns=on">Wearable</a></li>
              <li><a href="/teardowns/?q=&category=Tablet,+Computing+%26+Gaming&category=Accessory&show_only_full_teardowns=on">Tablet, Computing & Gaming</a></li>
            </ul>
          </div>
          <div id="menu-teardowns" class="secondary">
            <div class="title"><a href="/teardowns/?q=&category=ADAS&category=Electrification&category=Infotainment&category=Telematics&category=Other+ECUs&show_only_full_teardowns=on/">Automotive</a></div>
            <ul>
              <li><a href="/teardowns/?q=&category=ADAS&track=Automotive&show_only_full_teardowns=on">ADAS</a></li>
              <li><a href="/teardowns/?q=&category=Electrification&track=Automotive&show_only_full_teardowns=on">Electrification</a></li>
              <li><a href="/teardowns/?q=&category=Infotainment&track=Automotive&show_only_full_teardowns=on">Infotainment</a></li>
              <li><a href="/teardowns/?q=&category=Telematics&track=Automotive&show_only_full_teardowns=on">Telematics</a></li>
              <li><a href="/teardowns/?q=&category=Other+ECUs&show_only_full_teardowns=on">Other ECUs</a></li>
            </ul>
          </div>
          <div id="menu">
            <ul>
              <li><a href="{% url 'component_search' %}">Parts</a></li>
              <li><a href="{% url 'roadmap' %}">Roadmap</a></li>
              <li><a href="{% url 'note-listing' %}">Teardown Notes</a></li>
              <li><a href="{% url 'my_devices' %}">My Devices</a></li>
            </ul>
          </div>
          <div class="like-select out-exclude dropdown" id="header-dropdown">

            <div class="header-searchbox">
              <form class="navbar-form navbar-right" id="devices-search" method="get" action="/teardowns/search/">
                <div class="input-group search-box-top">
                  <input id="main-search-input" type="text" name="q" placeholder="Search..." />
                  <button class="search" type="submit"><i class="fa fa-search" aria-hidden="true"></i></button>
                </div>
              </form>
            </div>
            <div class="like-select-trigger">
              <div class="like-select-text" id="user-full-name">
                <i class="fa fa-user" aria-hidden="true"></i>
              </div>
              <span class="like-select-icon vsm-icon chevron-down-icon">

              </span>
            </div>
            <div class="like-select-options">
              <ul>
                <li><a href="{% url 'admin:index' %}">Admin</a></li>
                <li><a href="{% url 'accounts.views.account_list' %}">Activity</a></li>
                <li><a href="{% url 'phoneggs-conflicts' %}">Conflicts</a></li>
                <li><a href="{% url 'checkout.views.checkout' %}">Check Out</a></li>
                <li><a href="{% url 'my_devices' %}">My Devices</a></li>
                <li><a href="{% url 'teardown-map' %}">Map With Phonegg</a></li>
                <li><a href="{% url 'phoneggs-sync' %}">Sync With Phonegg</a></li>
                <li><a href="/accounts/contact">Contact Us</a></li>
                <li>
                  <a href="{% url 'logout' %}?next={{ request.path }}">Log Out</a>
                </li>
              </ul>
            </div>
            <ul class="login-right">
              <li><a href="/accounts/contact">Contact Us</a></li>
              <li><a href="{% url 'login' %}?next={{ request.path }}">Log In</a></li>
            </ul>
          </div>
        </div>

        <div>
          Layers
        </div>
      </React.Fragment >
    )
  }
}

export default Header;